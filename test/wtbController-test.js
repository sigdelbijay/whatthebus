'use strict';
describe("WtbController", function(){
	var $rootScope,
	$httpBackend,
	createController;

	beforeEach(function(){
		module('wtbApp');

		inject(function($injector){
			$rootScope = $injector.get('$rootScope');
			$httpBackend = $injector.get('$httpBackend');
			$httpBackend.when('GET', '/cities').respond('hello');
			var $controller = $injector.get('$controller');

			createController = function() {
				return $controller('WtbController', {'$scope' : $rootScope });
			};
		});
	});

	it('should make a HTTP GET request', function() {
		$httpBackend.expectGET('/cities');
		var controller = createController();
		$httpBackend.flush();
		console.log('$rootScope.data =======>' + $rootScope.data);
		expect($rootScope.data).toEqual('hello');
	});
});