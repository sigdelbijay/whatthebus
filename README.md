###What The Bus(wtb-web) - AngularJS Web Application
>###Stack
* Angular JS on the client side.
* Route using express.js(node.js framework).
* CSS based on Twitter's Bootstrap and custom styles.

>###Build
The project is built on AngularJS and is integrated with other tools commonly used in AngularJS community:
* Test written using Jasmine syntax.
* Test are executed by Karma Test Runner.
* Build supporting JS, CSS and AngularJS templates minification.
 
>###Installation
####Platform and Tools
You need to install Node.js and then the development tools. Node.js comes with a package manager called npm for installing NodeJS applications and libraries.  
* Install node.js`
* Install GRUNT-cli and karma as global npm modules  
  `npm install -g grunt-cli`

>####Get The Code
Either clone this repository or fork it on GitHub and clone your fork:  
`git clone https://github.com/WhatTheBus/wtb-web.git`  
`cd wtb-get`

>####Install Local Dependencies
`npm install`  
`npm install bower`

>###Running
>####Start the Server
>#####Run the Server
* `node app.js`  
* Browse to the application at _http://localhost:3030_
  
>#####Browser Support
The Application has been tested on Google Chrome and Mozilla Firefox however most modern browsers that support AngularJS Framework should be compatiable.
    
>####Continuous Testing
We have implemented karma for continuously watching for the file changes and automatically run all the test on every change without rebuilding the distribution files. For running the test codes:  
`Run karma start karma.conf.js`