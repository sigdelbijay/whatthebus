function sticky_relocate() {
    var window_top = $(window).scrollTop();
    var div_top = $('#sticky-anchor').offset().top;
    if (window_top > div_top) {
        $('#sticky-navbar').addClass('navbar-fixed-top');
        $('#search-bar').addClass('search-bar-fixed-top');
        $('#sticky-navbar').removeClass('navbar-bottom');
    } else {
        $('#sticky-navbar').addClass('navbar-bottom');
        $('#sticky-navbar').removeClass('navbar-fixed-top');
        $('#search-bar').removeClass('search-bar-fixed-top');
    }
    var srch_top = div_top * 0.5;
    console.log(window_top+" "+ srch_top);
    if (window_top > srch_top) {
      $('#search-bar').removeClass('sticky-search-bar');
    }else {
      $('#search-bar').addClass('sticky-search-bar');
    }
}

$(function () {
    $(window).scroll(sticky_relocate);
    sticky_relocate();
});
