'use strict';

var app = angular.module("wtbApp",['ngMaterial', 'ui.router']).
config(function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/');
	$stateProvider
		.state('home', {
			url: '/',
			templateUrl: '/partials/home'
		})
		.state('search', {
			url: '/search',
			templateUrl: '/partials/search'
		})
		.state('searchResult', {
			url: '/searchResult',
			templateUrl: '/partials/searchResult'
		})
}).

controller("WtbController", function($scope, $http, $state){
	$http.get('/cities').then(function(response){
		$scope.data = response.data;
	})
	console.log('aayo');
	$scope.noOfTickets = ['1', '2', '3', '4', '5', '6'];
	$scope.searchBus = function(){
		var data = {
			'depart-date':moment($scope.selectedDate).format('DD-MM-YYYY'),
			from: $scope.from.id,
			to: $scope.to.id,
			pax: $scope.selectedTicketNo
		};
		$http.get('/searchBus', {params: data}).then(function(response){
			var searchData;
			searchData = JSON.parse(response.data.body);
			$scope.searchData = [];
			searchData.forEach(function(entry){
				var availabeData = {
					id : entry.id,
					operatorName : entry.operatorName,
					arrivalDate : moment(entry.arrivalTime).format('DD MMM YYYY'),
					arrivalTime : moment(entry.arrivalTime).format('hh : mm') ,
					fromCity : entry.fromCity,
					duration : moment.duration(entry.departTime-entry.arrivalTime).minutes(),
					toCity : entry.toCity,
					departDate : moment(entry.departTime).format('DD MMM YYYY'),
					departTime : moment(entry.departTime).format('hh : mm')
				}
				$scope.searchData.push(availabeData)
			})

		})

	}

	$scope.fetchSearchPage = function(){
		$state.go('search');
	}
})
