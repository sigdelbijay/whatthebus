'use strict';

var express = require('express'),
	logger = require('morgan'),
	bodyParser = require('body-parser'),
	util = require('util');
var app = express();
require('./routes')(app);
var config = require('./config');

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(__dirname + '/public'));

var router=express.Router();
app.use('/', router);

app.set('port', config.port || 3000);
var server = app.listen(app.get('port'), function() {
    util.log('WhatTheBus web server listening on port ' + server.address().port);
});