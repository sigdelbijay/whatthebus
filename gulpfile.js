var gulp = require('gulp');
var sass = require('gulp-sass');
var cssbeautify = require('gulp-cssbeautify');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');


// gulp.task('css', function() {
//     return gulp.src('./public/css/*.css')
//         .pipe(autoprefixer({
//             browsers: ['last 2 versions'],
//             cascade: false
//         }))
//         .pipe(gulp.dest('./public/css/'));
// });
gulp.task('beauty', function() {
    return gulp.src('./public/css/*.css')
    .pipe(cssbeautify({
        indent: '  ',
        openbrace: 'separate-line',
        autosemicolon: true
    }))
    .pipe(gulp.dest('./public/css/'));
});
gulp.task('styles', function() {
    gulp.src('public/sass/*.scss')
    		.pipe(plumber())
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./public/css/'))
});
gulp.task('default',function() {
    gulp.watch('public/sass/*.scss',['styles','beauty']);
});
