'use strict';

var app = require('express')();
var apibase;

// dev settings
if ('development' === app.get('env')) {
    apibase = 'http://localhost:3030';
}

/*prod settings
if ('production' === app.get('env')) {
    apibase = 'http://www.whatthebus.com:3030';
}*/

module.exports = {
    port: 3030,
    apibase: apibase
};
