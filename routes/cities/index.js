'use strict';

var mw = require('../middleware');
var cities = require('./cities');
module.exports = function(app){
	app.get('/cities', cities.fetch, mw.respond, mw.error);

}