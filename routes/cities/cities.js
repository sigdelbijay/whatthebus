var mw = require('../middleware');
var makeApiCall = require('../lib/make-api-call');
/*var config = require('../../config');
var apibase = config.apibase;*/
var cities = {
	fetch : function fetchFN(req, res, next){

		var api = 'http://www.api-wtb.elasticbeanstalk.com/cities';
		makeApiCall(api, 'get', '', respond);
		function respond(err, result){
			if(err) next(err);
			req.cdata = result;
			next();
		}
	}
}
module.exports = cities;