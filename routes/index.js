'use strict';

module.exports = function (app){
	require('./cities')(app);
	require('./bus')(app);
	app.get('/', function (req, res) {
	  res.render('index');
	});
	app.get('/partials/home', function (req, res) {
	  res.render('partials/home');
	});
	app.get('/partials/search', function(req, res){
		res.render('partials/search');
	});
	app.get('/partials/searchResult', function(req, res){
		res.render('partials/searchResult');
	});
}
