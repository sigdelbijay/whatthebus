'use strict';

var mw = require('../middleware');
var bus = require('./bus');
module.exports = function(app){
	app.get('/searchBus', bus.fetch, mw.respond, mw.error);
}