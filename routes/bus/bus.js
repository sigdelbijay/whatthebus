'use strict';

var mw = require('../middleware');
var makeApiCall = require('../lib/make-api-call');
var request = require('request');
var bus = {
	fetch : function fetchFN(req, res, next){
		var data = {
			'depart-date' :req.query['depart-date'],
			from : req.query.from,
			to : req.query.to,
			pax : req.query.pax
		};

		var url = {
			endpoint: 'http://www.api-wtb.elasticbeanstalk.com/trips',
			method: 'GET'
		};


		var reqOptions = {
			url: url.endpoint,
			qs: data,
			method: url.method,
			timeout: 30 * 1000
		};

		request(reqOptions, function(err, res) {
			if(err) return next(err);
			req.cdata = res;
			next();
		});

	}
}
module.exports = bus;